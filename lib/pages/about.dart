import 'package:flutter/material.dart';
import 'package:path/path.dart';

class Body extends StatelessWidget {
  const Body({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column();
  }
}

class About extends StatelessWidget {
  const About({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('เกี่ยวกับเรา'),
        backgroundColor: Colors.pink,
      ),
      body: Column(
        children: [
          image(
            assetImage: AssetImage("assets/images/6250110007.jpg"),
          ),
          SizedBox(height: 19),
          name(
            text: Text(
              "นายพัชรพล จันชนะพล \n 6250110007 ICM",
              style: TextStyle(fontSize: 20,color: Colors.white),
            ),
          ),
          SizedBox(height: 50),
          image(
            assetImage: AssetImage("assets/images/6250110010.jpg"),
          ),
          SizedBox(height: 19),
          name(
            text: Text(
              "นางสาวรัชนีกร สุกใส \n 6250110010 ICM",
              style: TextStyle(fontSize: 20, color: Colors.white),
            ),
          ),
        ],
      ),
    );
  }
}

class name extends StatelessWidget {
  const name({Key? key, required this.text}) : super(key: key);

  final Text text;
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
      child: FlatButton(
        padding: EdgeInsets.all(20),
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
        color: Colors.pink,
        onPressed: () {},
        child: Row(
          children: [
            Image.asset(
              "assets/images/user.png",
              width: 50,
            ),
            SizedBox(width: 30),
            Expanded(
              child: text,
            ),
          ],
        ),
      ),
    );
  }
}

class image extends StatelessWidget {
  const image({Key? key, required this.assetImage}) : super(key: key);

  final AssetImage assetImage;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 20),
      child: SizedBox(
        height: 115,
        width: 115,
        child: CircleAvatar(
          backgroundImage: assetImage,
        ),
      ),
    );
  }
}
