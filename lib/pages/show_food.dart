import 'dart:io';

import 'package:flutter/material.dart';
import 'package:project_sqlite/database/database_helper.dart';
import 'package:project_sqlite/model/food_model.dart';

class ShowFood extends StatelessWidget {
  final id;
  ShowFood({Key? key, required this.id}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('อาหารแปลกๆ'),
      ),
      body: Center(
        child: FutureBuilder<List<FoodMed>>(
            future: DatabaseHelper.instance.getProfile(this.id),
            builder: (BuildContext context,
                AsyncSnapshot<List<FoodMed>> snapshot) {
              if (!snapshot.hasData) {
                return Center(child: Text('รอสักครู่'));
              }
              return snapshot.data!.isEmpty
                  ? Center(child: Text('ไม่พบรายการ'))
                  : ListView(
                      children: snapshot.data!.map((food) {
                        return Center(
                          child: Column(
                            children: [
                              SizedBox(
                                height: 90,
                              ),
                              CircleAvatar(
                                backgroundImage: FileImage(File(food.image)),
                                radius: 150,
                              ),
                              SizedBox(
                                height: 60,
                              ),
                              Text(
                                '${food.name}',
                                style: TextStyle(
                                    fontSize: 26, fontWeight: FontWeight.bold),
                              ),
                              SizedBox(
                                height: 20,
                              ),
                              Text('ราคา: ${food.price}',
                                  style: TextStyle(
                                    fontSize: 20,
                                  )),
                            ],
                          ),
                        );
                      }).toList(),
                    );
            }),
      ),
    );
  }
}
