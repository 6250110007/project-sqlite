import 'package:flutter/material.dart';
import 'package:project_sqlite/pages/add_food.dart';
import 'package:project_sqlite/pages/home_page.dart';

class App extends StatelessWidget {
  const App({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'อาหารแปลก',
      theme: ThemeData(
        primarySwatch: Colors.pink,
      ),
      home: const MyHomePage(title: 'อาหารแปลกๆ'),
    );
  }
}
