class FoodMed {
  int? id;
  String name;
  String price;
  String image;

  FoodMed({
    this.id,
    required this.name,
    required this.price,
    required this.image,
  });

  factory FoodMed.fromMap(Map<String, dynamic> json) => new FoodMed(
        id: json['id'],
        name: json['name'],
        price: json['price'],
        image: json['image'],
      );

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'name': name,
      'price': price,
      'image': image,
    };
  }
}
